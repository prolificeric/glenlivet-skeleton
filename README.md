# Glenlivet Project Skeleton

TODO: Fill in the sections!

## Configuration
### Dependency Injection

## Project Structure
### Versioning

## Site Facade

## Models
### Parsing From HTML

## Routes

## Sessions
### Behalf

## Documentation

## Testing
### Parser Unit Tests
### Functional Tests
