'use strict';

const express = require('express');
const behalf = require('behalf');
const Site = require('./lib/site');

// Set up application objects
const config = require('./config');
const app = express();
const site = new Site(config.site);
const sessionStore = new behalf.stores.Redis({ config: config.redis });
const application = { config, app, site, sessionStore };

// Versions are simply different routers that get mounted to main app object
app.use('/v1', require('./v1/routes')(application));

app.listen(config.port, () => {
  console.log(`Server listening on port ${config.port}`);
});
