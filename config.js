'use strict';

// Some config management systems use environment variables.
const env = process.env;

module.exports = {
  port: env.PORT,
  site: {
    baseUrl: env.SITE_BASE_URL
  },
  redis: {
    host: env.REDIS_HOST,
    port: env.REDIS_PORT
  }
};
