'use strict';

const express = require('express');
const Product = require('../models/Product');
const { endpoint } = require('../../lib/middleware');

module.exports = function ({ site }) {
  const router = express.Router();

  router.get('/',
    endpoint(req => {
      return site
        .search(req.query)
        .get('body')
        .then(Product.fromSearchResults);
    })
  );

  return router;
};
