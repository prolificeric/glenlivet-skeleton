'use strict';

const express = require('express');
const behalf = require('behalf');
const jsonParser = require('body-parser').json;

module.exports = function (application) {
  const router = express.Router();
  const sessionManager = behalf.middleware.sessionManager({
    store: application.sessionStore,
    host: application.config.site.baseUrl,
    getKey: req => req.headers.sessionid
  });

  router.use(sessionManager);
  router.use(jsonParser());
  router.use('/products', require('./products')(application));
  router.use('/login', require('./login')(application));

  return router;
};
