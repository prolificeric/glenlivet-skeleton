'use strict';

const express = require('express');
const { requireSession } = require('behalf').middleware;
const { endpoint } = require('../../lib/middleware');

module.exports = function ({ site }) {
  const router = express.Router();

  router.post('/',
    requireSession(),
    endpoint(req => {
      return site
        .login(req.body, req.behalf.session)
        .then(response => {
          return {
            success: response.statusCode.toString() === '200'
          };
        });
    })
  );

  return router;
};
