'use strict';

const Product = require('../models/product');

const mockSearchResults = `
  <ul class="products">
    <li class="product" data-id="prod12345">
      <div class="name">Sample Product</div>
    </li>
    <li class="product" data-id="prod12345">
      <h2>Sample Product 2</h2>
    </li>
  </ul>
`;

describe('Product model', () => {
  describe('parsing search results', () => {
    const parsed = Product.fromSearchResults(mockSearchResults);

    it('finds all products', () => {
      return parsed.tap(products => {
        products.length.should.equal(2);
      });
    });

    it('extracts the id', () => {
      return parsed.tap(products => {
        products[0].id.should.equal('prod12345');
      });
    });

    it('extracts the name from div.name if it exists', () => {
      return parsed.tap(products => {
        products[0].name.should.equal('Sample Product');
      });
    });

    // Example of testing edge cases using the mock:
    it('extracts the name from the first h2 if it exists', () => {
      return parsed.tap(products => {
        products[1].name.should.equal('Sample Product 2');
      });
    });
  });
});
