'use strict';

const htmlToJson = require('html-to-json');

class Product {
  constructor(data) {
    Object.assign(this, {
      id: null,
      name: null
    }, data);
  }

  static fromSearchResults(html) {
    return htmlToJson
      .parse(html, ['.product', {
        'id': $product => {
          return $product.attr('data-id') || null;
        },
        'name': $product => {
          return $product.find('.name').text()
            || $product.find('> h2').eq(0).text()
            || null;
        }
      }])
      .map(data => new Product(data));
  }
}

module.exports = Product;
