'use strict';

const bluebird = require('bluebird');

module.exports = {
  endpoint(handler) {
    return function (req, resp, next) {
      bluebird
        .resolve(handler(req, resp))
        .tap(resp.send.bind(resp))
        .catch(next);
    };
  }
};
