'use strict';

const { requestAsync } = require('./helpers');

class Site {
  constructor(options) {
    Object.assign(this, {
      baseUrl: null
    }, options);
  }

  search({ keywords, limit, offset }) {
    return this.request({
      uri: '/catalog/search.jsp',
      qs: {
        q: keywords,
        total: limit,
        skip: offset
      }
    });
  }

  login({ username, password }, session) {
    return this.request({
      method: 'POST',
      uri: '/login',
      form: {
        un: username,
        pw: password
      }
    }, session);
  }

  request(options, session) {
    if (typeof options === 'string') {
      options = { uri: options };
    }

    options = Object.assign({
      baseUrl: this.baseUrl
    }, options);

    return session?
      session.request(options):
      requestAsync(options);
  }
}

module.exports = Site;
