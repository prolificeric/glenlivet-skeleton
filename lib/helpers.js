'use strict';

const bluebird = require('bluebird');
const request = require('request');

module.exports = {
  requestAsync(options) {
    return bluebird.fromCallback(request.bind(null, options));
  }
};
